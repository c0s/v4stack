/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.v4stack

import java.util.jar.JarEntry
import java.util.jar.JarFile
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import java.util.zip.ZipInputStream
import java.util.List
import java.net.URL

/**
 * Class provides a set of primitives to work with Maven provided
 * artifact packages as jar files.
 */
class JarTools {
  static private Log LOG = LogFactory.getLog(JarTools.class);

  // Exclude META* and inner classes
  final public static List<String> excludes = ['.*META.*', '.*\\$.*.class']
  static {
    // Instrumenting JDK libs, etc.
    MOPs.bootstrapMOPs();
  }

  private static List<String> exclude(final List<String> collection,
                                      final List<String> patterns) {
    List<String> ret = collection;
    if (collection == null) {
      return null;
    }
    def removing = [];

    patterns.each { pattern ->
      def p = ~/${pattern}/;
      collection.each {
        if (p.matcher(it).matches())
          removing.add(it);
      }
    }
    ret.removeAll(removing);
    return ret;
  }

  /**
   * Returns a list of jar entries limited only by default filter e.g.
   * without inner classes, and META-INF directory
   * @param jarName name of the jar file to list its content
   * @return list of jar entries
   */
  public static List<String> getContent(String jarName) {
    return exclude(unfilteredList(jarName), excludes);
  }

  /**
   * Returns a list of jar entries filtered by pattern
   * @param jarName name of the jar file to list its content
   * @param pattern to add on top of default filter, e.g. <code>.*Assembler.*</code>
   * @return list of jar entries
   */
  public static List<String> getContent(String jarName, String pattern) {
    def filters = excludes.clone(); // Make sure that default list is intact
    filters.add(pattern);
    return exclude(unfilteredList(jarName), filters);
  }

  /**
   * Method finds a jar file the given klass belongs to and returns its URL
   * @param klass ref to a class contained by a jar to be found
   * @return URL of the jar file found in the classpath; <code>null</code> if not found
   */
  public static URL getLocation(Class klass) {
    URL clsUrl = klass.getResource(klass.getSimpleName() + ".class");
    if (clsUrl != null) {
      try {
        URLConnection conn = clsUrl.openConnection();
        if (conn instanceof JarURLConnection) {
          JarURLConnection connection = (JarURLConnection) conn;
          return connection.getJarFileURL();
        }
      }
      catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
    return null;
  }
  /**
   * Method finds a jar file the given klass name belongs to and returns its URL
   * @param klassName a class name contained by a jar to be found
   * @return URL of the jar file found in the classpath; <code>null</code> if not found
   */
  public static URL getLocation(String klassName) throws ClassNotFoundException {
    Class klass = Class.forName(klassName);
    return getLocation(klass);
  }

  /**
   * Finds a jar file under given location using a regex
   * @param location of a base directory
   * @param includeRegEx regex to find a file by
   * @return name of the jar file location or <code>null</code> of not found
   */
  public static String findJar(String location, String includeRegEx) {
    return findAllJars(location, includeRegEx)[0];
  }

  /**
   * Finds all jar files recursively under given location matching a regex
   * @param location of a base directory
   * @param includeRegEx regex to find a file by
   * @return list of names of the jar file locations or <code>null</code> of not found
   */
  public static List<String> findAllJars(String location, String includeRegEx) {
    if (location == null || !new File(location).exists() || !new File(location).isDirectory()) {
      LOG.warn("$location isn't a directory or doesn't exist");
      return null;
    }
    return new File(location).list(
          [accept: {d, f -> f ==~ /$includeRegEx/ }] as FilenameFilter
    ).toList();
  }

  /**
   * Method to unpack a content from a jar file container for klass
   * @param klass ref to a class contained by a jar to be found
   * @param dest folder to unpack the content into
   * @param includes a substring used to find content to be extracted. Method uses
   * jarEntry.name.contains(includes) to find entries to be unpacked. It is not
   * a regular expression
   */
  public static void unpack(Class klass, String dest, String includes) {
    URL jarLoc = getLocation(klass);
    if (jarLoc == null) {
      throw new FileNotFoundException("Can't find jar file for $klass");
    }
    ZipInputStream zis =
      new ZipInputStream(jarLoc.openConnection().getInputStream());
    zis.unzip(dest, includes);
  }

  /**
   * Method to unpack a content from a jar file container for klass
   * @param klassName a class name contained by a jar to be found
   * @param dest folder to unpack the content into
   * @param includes a substring used to find content to be extracted. Method uses
   * jarEntry.name.contains(includes) to find entries to be unpacked. It is not
   * a regular expression
   */
  public static void unpack(String klassName, String dest, String includes) {
    Class klass = Class.forName(klassName);
    unpack(klass, dest, includes);
  }

  private static List<String> unfilteredList(String jarName) {
    def ret = [];
    JarFile jar;
    try {
      jar = new JarFile(jarName)
    } catch (Exception e) {
      LOG.warn("Can't open $jarName")
      return null;
    };
    Enumeration<JarEntry> entries = jar.entries();
    while (entries.hasMoreElements()) {
      def entry = entries.nextElement();
      if (entry.isDirectory()) {
        continue;
      }
      ret.add(entry.name);
    }
    return ret;
  }
}
