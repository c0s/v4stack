/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.v4stack

import java.util.zip.ZipInputStream
import java.util.zip.ZipEntry

class MOPs {
  static void bootstrapMOPs() {
    /**
     * MOP'ing (monkey patching) java.util.zip with built-in unpack functionality
     * @param dest directory where the content will be unpacked
     * @param includes regexps to include resources to be unpacked
     */
    ZipInputStream.metaClass.unzip = { String to, String includeRegEx ->
      if (includeRegEx == null) includeRegEx = "";
      final def zipBufferSize = 4096;
      def ret = delegate; // i.e. self object
      File destDir = new File(to);
      if (!destDir.exists()) {
        destDir.mkdir();
      }
      ret.withStream {
        ZipEntry entry;
        while (entry = ret.nextEntry) {
          if (!entry.name.contains(includeRegEx)) {
            continue
          };
          if (!entry.isDirectory()) {
            new File("${to}${File.separator}${entry.name}").parentFile?.mkdirs()
            def output =
              new FileOutputStream("${to}${File.separator}${entry.name}")
            output.withStream {
              int len;
              byte[] buffer = new byte[zipBufferSize]
              while ((len = ret.read(buffer)) > 0) {
                output.write(buffer, 0, len);
              }
            }
          }
          else {
            new File(to + File.separator + entry.name).mkdir()
          }
        }
      }
    }
  }
}
