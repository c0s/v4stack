/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.v4stack

import org.junit.Test
import static org.junit.Assert.assertTrue
import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertNull
import static org.junit.Assert.assertNotNull

class TestJarTools {
  @Test
  public void testGetContentNegative () {
    final def toolsJar = System.getenv('JAVA_HOME') + "/lib/nonexisting.jar";
    assertNull("Non-existing jar content isn't null",
            JarTools.getContent(toolsJar));
  }
  @Test
  public void testGetContent () {
    final def toolsJar = System.getenv('JAVA_HOME') + "/lib/tools.jar";
    def toolsEntries =
      JarTools.getContent(toolsJar);
    assertTrue("Expect non-empty content of \$JAVA_HOME/lib/tools.jar",
            toolsEntries.size() != 0);
    assertFalse("META-INF should be excluded from the content",
      toolsEntries.toString().contains('META-INF'));
    assertFalse("inner classes should be excluded from the content",
      toolsEntries.toString().contains('.*\\$.*.class'));

    def moreExcludes = ".*ConstantPool.*";
    toolsEntries = JarTools.getContent(toolsJar, moreExcludes);
    assertFalse("META-INF should be excluded from the content",
      toolsEntries.toString().contains('META-INF'));
    assertFalse("ConstantPool* should be excluded from the content",
      toolsEntries.toString().contains(moreExcludes));
  }

  @Test
  public void testGetLocation() {
    URL location = JarTools.getLocation(java.lang.String);
    assertNotNull("Should find java.lang.String's jar location", location);

    location = JarTools.getLocation("java.lang.String")
    assertNotNull("Should find java.lang.String's jar location", location);
  }

  @Test (expected=ClassNotFoundException.class)
  public void testGetLocationNeg() {
    def location = JarTools.getLocation("java.lang.NotAClass")
    assertNull("Shouldn't find non-existing class's jar location", location);
  }

  @Test
  public void testFindAllJars () {
    def jarPattern = ".*ols.jar"
    def baseDir = "${System.getenv('JAVA_HOME')}/lib"
    def paths =
      JarTools.findAllJars(baseDir, jarPattern)
    assertTrue("Should've found tools.jar file", paths[0].endsWith("tools.jar"));

    paths = JarTools.findAllJars(baseDir, "${jarPattern}.nop");
    assertTrue("Shouldn't have found non-existing jar file", paths[0] == null);
  }
  @Test
  public void testFindAllJar () {
    def jarPattern = ".*ols.jar"
    def baseDir = "${System.getenv('JAVA_HOME')}/lib"
    def path =
      JarTools.findJar(baseDir, jarPattern);
    assertTrue("Should've found tools.jar file", path.endsWith("tools.jar"));

    path = JarTools.findJar(baseDir, "${jarPattern}.nop");
    assertTrue("Shouldn't have found non-existing jar file", path == null);
  }

  @Test
  public void testUnpackAll() {
    File temp = File.createTempFile("temp.dir", null);
    temp.delete(); // Let get rid of the file so a temp.dir can be created there
    JarTools.unpack(Class.forName("javax.jnlp.FileContents"), temp.getAbsolutePath(), null);
    assertTrue ("Shouldn't be empty", temp.listFiles().length != 0);
    temp.delete(); // Make sure the stuff is removed at the end
  }

  @Test
  public void testUnpackPattern() {
    File temp = File.createTempFile("temp.dir", null);
    temp.delete(); // Let get rid of the file so a temp.dir can be created there
    JarTools.unpack(Class.forName("javax.jnlp.FileContents"), temp.getAbsolutePath(),
            "FileOpenServiceImpl");
    assertTrue ("Shouldn't be empty", temp.listFiles().length != 0);
    temp.delete(); // Make sure the stuff is removed at the end
  }

  @Test
  public void testUnpackAllString() {
    File temp = File.createTempFile("temp.dir", null);
    temp.delete(); // Let get rid of the file so a temp.dir can be created there
    JarTools.unpack(Class.forName("javax.jnlp.FileContents"), temp.getAbsolutePath(), null);
    assertTrue ("Shouldn't be empty", temp.listFiles().length != 0);
    temp.delete(); // Make sure the stuff is removed at the end
  }

  @Test
  public void testUnpackPatternString() {
    File temp = File.createTempFile("temp.dir", null);
    temp.delete(); // Let get rid of the file so a temp.dir can be created there
    JarTools.unpack(Class.forName("javax.jnlp.FileContents"), temp.getAbsolutePath(),
            "FileOpenServiceImpl");
    assertTrue ("Shouldn't be empty", temp.listFiles().length != 0);
    temp.delete(); // Make sure the stuff is removed at the end
  }
}
